const express = require("express");
const app = express();
const path = require("path");
const router = express.Router();
const helmet = require('helmet');
const mysql = require('mysql');
var session = require('express-session')
//var client = require('redis').createClient()
var crypto = require('crypto');


app.set('trust proxy', 1) // trust first proxy
app.use(session({
  secret: 's3Cur3',
  name: 'sessionId'
}))
app.use(helmet());
app.disable('x-powered-by')
app.use(express.urlencoded({ extended: true }));
const pool = mysql.createPool({
    connectionLimit : 100, //important
    host     : '',
    user     : '',
    password : '',
    database : 'unsaferSite',
    debug    :  false
});
app.set("view engine", "pug");  
app.set("views", path.join(__dirname, "views"));
app.use(express.static('public'))

router.get("/", (req, res) => {
  res.render("index");
});
router.get("/about", (req, res) => {
  res.render("about", { title: "A propos", message: "Page a propos de ce site" });
});
router.get("/login", (req, res) => {
  res.render("login");
});
router.post("/login", (req, res) => {
  let requests= "SELECT * FROM site_user where name=? and password=?;"
  var hasher = crypto.createHash("md5");
  hasher.update(req.body.psw);
  encryptString= hasher.digest('hex'); 

  requests = mysql.format(requests, [req.body.uname, encryptString])

  pool.query(requests,(err, data) => {
    if(err) {
      res.status(500)
      res.render("login", {errorMessage:"Erreur interne, reassayer ultérieurement..."});
      return;
    }
    if( data.length > 0){
      req.session.user=data
      res.redirect("secured");
    }else{
      res.status(401)
      res.render("login", {errorMessage:"nom d'utilisateur ou mot de passe incorrect"});
    }
  });
});
app.get("/secured", (req, res)=>{
  if(req.session.user==null){
    res.redirect("/login")
  }else{
    res.render("secured")
  }
})
app.use("/", router);
app.listen(process.env.port || 3000);
