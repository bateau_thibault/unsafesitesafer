FROM node:slim

WORKDIR /usr/app/
COPY . /usr/app/
RUN npm install

CMD ["node", "app.js"]
EXPOSE 3000